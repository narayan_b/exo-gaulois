USE asterix;
-- 1. Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes)
SELECT "1. Liste des potions : Numéro, libellé, formule et constituant principal." as "Question - 1";
SELECT NumPotion, LibPotion, Formule, ConstituantPrincipal FROM potion;


-- 2. Liste des noms des trophées rapportant 3 points. (2 lignes)
SELECT "2. Liste des noms des trophées rapportant 3 points." as "Question - 2";
SELECT NomCateg, FROM categorie WHERE NbPoints = 3; 


-- 3. Liste des villages (noms) contenant plus de 35 huttes. (4 lignes)
SELECT "3. Liste des villages (noms) contenant plus de 35 huttes. (4 lignes)" as "Question - 3";
SELECT NomVillage FROM village WHERE NbHuttes > 35;


-- 4. Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)
SELECT "4. Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)" as "Question - 4";
select * From trophee where DatePrise between '2052-05-01' and '2052-06-30';


-- 5.Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)
SELECT "5.Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)" as "Question - 5";
SELECT Nom FROM habitant WHERE Nom LIKE 'A%r%';


-- 6. Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)
SELECT "6. Numéros des habitants ayant bu les potions numéros 1, 3 ou 4." as "Question - 6";
SELECT DISTINCT (NumHab) FROM absorber WHERE NumPotion IN (1, 3, 4);


-- 7. Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10 lignes)
SELECT "7. Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10 lignes)" as "Question - 7";
SELECT trophee.NumTrophee, trophee.DatePrise, categorie.NomCateg, habitant.Nom
FROM trophee
JOIN categorie ON trophee.CodeCat = categorie.CodeCat
JOIN habitant ON trophee.NumPreneur = habitant.NumHab;


-- 8. Nom des habitants qui habitent à Aquilona. (7 lignes)
SELECT "8. Nom des habitants qui habitent à Aquilona. (7 lignes)" as "Question - 8";
SELECT habitant.nom FROM habitant
JOIN village ON habitant.numVillage = village.numVillage
WHERE village.NomVillage =  "Aquilona";


-- 9. Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)
SELECT "9. Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)" as "Question - 9";
SELECT habitant.Nom FROM habitant
JOIN trophee ON habitant.NumHab = trophee.NumPreneur
JOIN categorie ON trophee.CodeCat = categorie.CodeCat
WHERE categorie.NomCateg = 'Bouclier de Légat';


-- 10. Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant principal. (3 lignes)
SELECT "10. Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant principal. (3 lignes)" as "Question - 10";

SELECT potion.LibPotion, potion.Formule, potion.ConstituantPrincipal
FROM potion
JOIN fabriquer ON potion.NumPotion = fabriquer.NumPotion
JOIN habitant ON fabriquer.NumHab = habitant.NumHab
WHERE habitant.Nom = "Panoramix";


-- 11. Liste des potions (libellés) absorbées par Homéopatix. (2 lignes)
SELECT "11. Liste des potions (libellés) absorbées par Homéopatix. (2 lignes)" as "Question - 11";
SELECT DISTINCT (potion.LibPotion) 
FROM potion
JOIN absorber ON potion.NumPotion = absorber.NumPotion
JOIN habitant ON absorber.NumHab = habitant.NumHab
WHERE habitant.Nom = "Homéopatix";


-- 12. Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3. (4 lignes)
SELECT "12. Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3. (4 lignes)" as "Question - 12";
SELECT DISTINCT (habitant.Nom)
FROM habitant
JOIN absorber ON absorber.NumHab = habitant.NumHab
JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion
WHERE fabriquer.NumHab = 3;


-- 13. Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)
SELECT "13. Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)" as "Question - 13";
SELECT DISTINCT (habitant.Nom) 
FROM habitant
JOIN absorber ON absorber.NumHab = habitant.NumHab
JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion
WHERE absorber.NumPotion IN
(SELECT fabriquer.NumPotion FROM fabriquer JOIN habitant ON fabriquer.NumHab = habitant.NumHab
WHERE habitant.Nom = "Amnésix");


-- 14. Nom des habitants dont la qualité n'est pas renseignée. (2 lignes)
SELECT "14. Nom des habitants dont la qualité n'est pas renseignée. (2 lignes)" as "Question - 14";
SELECT Nom
FROM habitant
WHERE NumQualite IS NULL;

-- 15.Nom des habitants ayant consommé la potion magique n°1 (c`\'`est le libellé de la potion) en février 52. (3 lignes)
SELECT "15.Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la potion) en février 52. (3 lignes)" as "Question - 15";
SELECT DISTINCT (habitant.Nom) 
FROM habitant
JOIN absorber ON absorber.NumHab = habitant.NumHab
JOIN potion ON potion.NumPotion = absorber.NumPotion
WHERE LibPotion = "Potion magique n°1" AND MONTH(absorber.DateA) = 2 AND YEAR(absorber.DateA) = 2052;

-- 16. Nom et âge des habitants par ordre alphabétique. (22 lignes)
SELECT "16. Nom et âge des habitants par ordre alphabétique. (22 lignes)" as "Question - 16";
SELECT Nom, Age FROM habitant ORDER BY Nom ASC;

-- 17. Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village. (3 lignes)
SELECT "17. Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village. (3 lignes)" as "Question - 17";
SELECT resserre.NomResserre, village.NomVillage 
FROM resserre 
JOIN village ON village.NumVillage = resserre.NumVillage
ORDER BY resserre.Superficie DESC;

-- 18. Nombre d'habitants du village numéro 5. (4)
SELECT "18. Nombre d'habitants du village numéro 5. (4)" as "Question - 18";
SELECT COUNT(*) as "Result"
FROM habitant
WHERE NumVillage = 5;

-- 19. Nombre de points gagnés par Goudurix. (5)
SELECT "19. Nombre de points gagnés par Goudurix. (5)" as "Question - 19";
SELECT SUM(categorie.NbPoints) as 'Result'
FROM categorie
JOIN trophee ON trophee.CodeCat = categorie.CodeCat 
JOIN habitant ON trophee.NumPreneur = habitant.NumHab 
WHERE habitant.Nom = "Goudurix";


-- 20. Date de première prise de trophée. (03/04/52)
SELECT "20. Date de première prise de trophée. (03/04/52)" as "Question - 20";
SELECT MIN(DatePrise) as 'Result' FROM trophee;


-- 21. Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)
SELECT "21. Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)" as "Question - 21";
SELECT SUM(absorber.Quantite) as 'Result'
FROM absorber
JOIN potion ON potion.NumPotion = absorber.NumPotion
WHERE potion.LibPotion = 'Potion magique n°2';

-- 22. Superficie la plus grande. (895)
SELECT "22. Superficie la plus grande. (895)" as "Question - 22";
SELECT MAX(Superficie) as "Result" FROM resserre;


-- 23. Nombre d'habitants par village (nom du village, nombre). (7 lignes)
SELECT "23. Nombre d'habitants par village (nom du village, nombre). (7 lignes)" as "Question - 23";
SELECT village.NomVillage, COUNT(habitant.NumHab) AS 'Result'
FROM village
LEFT JOIN habitant ON village.NumVillage = habitant.NumVillage
GROUP BY village.NomVillage;


-- 24. Nombre de trophées par habitant (6 lignes)
SELECT "24. Nombre de trophées par habitant (6 lignes)" as "Question - 24";
SELECT habitant.Nom, COUNT(trophee.NumTrophee) AS "Result"
FROM habitant
RIGHT JOIN trophee ON habitant.NumHab = trophee.NumPreneur
GROUP BY habitant.Nom;


-- 25. Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes)
SELECT "25. Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes)" as "Question - 25";
SELECT province.NomProvince, AVG(habitant.Age) AS AverageAge
FROM habitant
JOIN village ON habitant.NumVillage = village.NumVillage
JOIN province ON village.NumProvince = province.NumProvince
GROUP BY province.NomProvince;


-- 26. Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9 lignes)
SELECT "26. Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9 lignes)" as "Question - 26";

SELECT habitant.Nom, COUNT(DISTINCT absorber.NumPotion) AS NombreDePotions
FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
GROUP BY habitant.Nom;

-- 27. Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)
SELECT "27. Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)" as "Question - 27";
SELECT habitant.Nom
FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
JOIN potion ON absorber.NumPotion = potion.NumPotion
WHERE potion.LibPotion = 'potion zen' AND absorber.Quantite > 2;


-- 28. Noms des villages dans lesquels on trouve une resserre (3 lignes)
SELECT "28. Noms des villages dans lesquels on trouve une resserre (3 lignes)" as "Question - 28";
SELECT DISTINCT village.NomVillage
FROM village
INNER JOIN resserre ON village.NumVillage = resserre.NumVillage;


-- 29. Nom du village contenant le plus grand nombre de huttes. (Gergovie)
SELECT "29. Nom du village contenant le plus grand nombre de huttes. (Gergovie)" as "Question - 29";

SELECT village.NomVillage
FROM village
WHERE village.NbHuttes = (SELECT MAX(NbHuttes) FROM village);


-- 30.Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes).
SELECT "30.Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes)." as "Question - 30";
SELECT habitant.Nom
FROM habitant
LEFT JOIN trophee ON habitant.NumHab = trophee.NumPreneur
WHERE habitant.Nom <> 'Obélix'
GROUP BY habitant.Nom
HAVING COUNT(trophee.NumTrophee) > (SELECT COUNT(trophee.NumTrophee) FROM habitant
LEFT JOIN trophee ON habitant.NumHab = trophee.NumPreneur
WHERE habitant.Nom = 'Obélix');